#include "TJAlienCredentials.h"
#include <cerrno>
#include <cstdlib>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <openssl/pem.h>
#include <openssl/x509.h>

using std::endl;
using std::getenv;
using std::ifstream;
using std::ofstream;
using std::stringstream;

const char *TJAlienCredentials::ENV_JOBTOKEN_KEY = "JALIEN_TOKEN_KEY";
const char *TJAlienCredentials::ENV_JOBTOKEN_CERT = "JALIEN_TOKEN_CERT";

const char *TJAlienCredentials::TMP_JOBTOKEN_KEY_FNAME_PREFIX =
    "tmpjobtokenkey_";
const char *TJAlienCredentials::TMP_JOBTOKEN_CERT_FNAME_PREFIX =
    "tmpjobtokencert_";

bool fileExists(const string &filename) {
  bool fileExists = false;
  FILE *f = fopen(filename.c_str(), "r");

  if (f != NULL) {
    fclose(f);
    fileExists = true;
  } else {
    fileExists = false;
  }

  return fileExists;
}

bool TJAlienCredentialsObject::exists() {
  return fileExists(certpath) && fileExists(keypath);
}

void TJAlienCredentials::writeSafeFile(const string &filename,
                                       const string &content) {
  int gDebug = std::getenv("gDebug") ? std::stoi(std::getenv("gDebug")) : 0;
  if (gDebug)
    INFO("writing safe file " << filename.c_str());
  int fd = open(filename.c_str(), O_RDWR | O_CREAT, 0600);
  if (write(fd, content.c_str(), content.length()) == -1)
    if (gDebug)
      ERROR("writing safe file failed: " << std::strerror(errno));
  close(fd);
}

string TJAlienCredentials::getSafeFilename(const string &prefix) {
  // creates a unique tmp filename for credentials following pattern
  // TmpDir/prefix_UNIQUESESSION
  //
  // (std::tmpnam, std::tmpfile could be used but don't follow the pattern or logic).
  // Instead construct UNIQUESESSION based on linux pid + random session number + instance counter.

  string filename_base = TJAlienCredentials::getTmpDir() + "/" + prefix, filename;
  pid_t pid = getpid();
  static uint session = 0; // random session identifier (to avoid collisions between multiple containers sharing /tmp)
  if (session == 0) {
    ifstream urandom("/dev/urandom", std::ios::in|std::ios::binary);
    if (urandom) {
      urandom.read(reinterpret_cast<char*>(&session), sizeof(session));
      urandom.close();
    }
  }
  static int instance = 0; // instance within a session
  do {
    instance += 1;
    filename = filename_base + std::to_string(pid) + "_" + std::to_string(session) + "_" + std::to_string(instance);
  } while (fileExists(filename));
  return filename;
}

std::string TJAlienCredentials::getTmpDir() {
  std::string tmpdir;

  if (getenv("TMPDIR") != NULL)
    tmpdir = getenv("TMPDIR");
  else if (getenv("TMP") != NULL)
    tmpdir = getenv("TMP");
  else if (getenv("TEMP") != NULL)
    tmpdir = getenv("TEMP");
  else
    tmpdir = P_tmpdir;

  return tmpdir;
}

std::string TJAlienCredentials::getHomeDir() {
  std::string homedir;

  if (getenv("HOME") != NULL)
    homedir = getenv("HOME");
  else
    homedir = "~";

  return homedir;
}

std::string TJAlienCredentials::getTokencertPath() {
  std::stringstream tokencert_s;
  tokencert_s << tmpdir << "/tokencert_" << getuid() << ".pem";
  std::string tokencert = tokencert_s.str();
  std::string tokencertpath = std::getenv("JALIEN_TOKEN_CERT") ?: tokencert;

  return tokencertpath;
}

std::string TJAlienCredentials::getTokenkeyPath() {
  std::stringstream tokenkey_s;
  tokenkey_s << tmpdir << "/tokenkey_" << getuid() << ".pem";
  std::string tokenkey = tokenkey_s.str();
  std::string tokenkeypath = std::getenv("JALIEN_TOKEN_KEY") ?: tokenkey;

  return tokenkeypath;
}

std::string TJAlienCredentials::getUsercertPath() {
  std::string usercert = homedir + "/.globus/usercert.pem";
  std::string usercertpath = std::getenv("X509_USER_CERT") ?: usercert;
  return usercertpath;
}

string TJAlienCredentials::getUserkeyPath() {
  std::string userkey = homedir + "/.globus/userkey.pem";
  std::string userkeypath = std::getenv("X509_USER_KEY") ?: userkey;
  return userkeypath;
}

TJAlienCredentials::TJAlienCredentials() {
  tmpdir = getTmpDir();
  homedir = getHomeDir();
}

void TJAlienCredentials::loadCredentials() {
  removeCredentials(cJOB_TOKEN);
  found_credentials.clear();
  loadTokenCertificate();
  loadFullGridCertificate();
  loadJobTokenCertificate();
}

void TJAlienCredentials::loadTokenCertificate() {
  TJAlienCredentialsObject token_credentials(getTokencertPath(),
                                             getTokenkeyPath(), cJBOX_TOKEN);

  if (token_credentials.exists()) {
    found_credentials[cJBOX_TOKEN] = token_credentials;
  }
}

void TJAlienCredentials::loadFullGridCertificate() {
  TJAlienCredentialsObject grid_certificate(getUsercertPath(), getUserkeyPath(),
                                            cFULL_GRID_CERT);

  if (grid_certificate.exists()) {
    found_credentials[cFULL_GRID_CERT] = grid_certificate;
  }
}

void TJAlienCredentials::loadJobTokenCertificate() {
  const char *env_cert = getenv(ENV_JOBTOKEN_CERT);
  const char *env_key = getenv(ENV_JOBTOKEN_KEY);

  // if it doesn't have both environment variables
  if (!env_cert || !env_key) {
    return;
  }

  // environment variables contain valid filepaths instead of the actual token
  if (fileExists(env_cert) && fileExists(env_key)) {
    found_credentials[cJOB_TOKEN] =
        TJAlienCredentialsObject(env_cert, env_key, cJOB_TOKEN);
  } else {
    const string &tmpcertpath = getSafeFilename(TMP_JOBTOKEN_CERT_FNAME_PREFIX);
    writeSafeFile(tmpcertpath, env_cert);

    const string &tmpkeypath = getSafeFilename(TMP_JOBTOKEN_KEY_FNAME_PREFIX);
    writeSafeFile(tmpkeypath, env_key);

    found_credentials[cJOB_TOKEN] =
        TJAlienCredentialsObject(tmpcertpath, tmpkeypath, cJOB_TOKEN, true);
  }
}

bool TJAlienCredentials::has(CredentialsKind kind) const {
  return found_credentials.count(kind) == 1;
}

TJAlienCredentialsObject TJAlienCredentials::get(CredentialsKind kind) const {
  if (this->has(kind)) {
    return found_credentials.at(kind);
  } else {
    return TJAlienCredentialsObject();
  }
}

TJAlienCredentialsObject TJAlienCredentials::get() {
  if (this->has(cJOB_TOKEN)) {
    return this->get(cJOB_TOKEN);
  } else if (this->has(cJBOX_TOKEN)) {
    return this->get(cJBOX_TOKEN);
  } else if (this->has(cFULL_GRID_CERT)) {
    TJAlienCredentialsObject co = this->get(cFULL_GRID_CERT);
    if (co.password.empty())
      co.readPassword();
    return co;
  } else {
    ERROR("Failed to get any credentials");
    return TJAlienCredentialsObject();
  }
}

void TJAlienCredentials::removeCredentials(CredentialsKind kind) {
  if (this->has(kind)) {
    if (kind == cJOB_TOKEN)
      get(kind).wipe();

    found_credentials.erase(kind);
  }
}

short TJAlienCredentials::count() { return found_credentials.size(); }

std::string readFile(const char *filename) {
  std::string line;
  std::stringstream contents;
  std::ifstream f(filename);

  if (f.is_open()) {
    while (getline(f, line)) {
      contents << line << std::endl;
    }
  }
  return contents.str();
}

const std::string TJAlienCredentialsObject::getKey() {
  return readFile(keypath.c_str());
}

const std::string TJAlienCredentialsObject::getCertificate() {
  return readFile(certpath.c_str());
}

void TJAlienCredentialsObject::readPassword() {
  if (this->kind == cFULL_GRID_CERT &&
      this->getKey().find("ENCRYPTED") != std::string::npos) {
    printf("[Grid certificate password: ]");
    struct termios termold, termnew;
    tcgetattr(fileno(stdin), &termold);
    termnew = termold;
    termnew.c_lflag &= ~ECHO;
    termnew.c_lflag |= ECHONL;
    tcsetattr(fileno(stdin), TCSANOW, &termnew);

    char password[64];
    if (fgets(password, sizeof(password), stdin) != nullptr) {
      tcsetattr(0, TCSANOW, &termold);

      password[strlen(password) - 1] = 0;
      this->password = std::string(password);
    } else {
      ERROR("Error while reading from stdin");
      this->password = "";
    }
  } else
    this->password = "";
}

const std::string TJAlienCredentialsObject::getPassword() {
  if (this->password.empty())
    readPassword();

  return this->password;
}

TJAlienCredentials::~TJAlienCredentials() {
  removeCredentials(cJOB_TOKEN);
}

void TJAlienCredentials::selectPreferedCredentials() {
  msg = "";

  if(has(cJOB_TOKEN)) {
    preferedCredentials = cJOB_TOKEN;
    return;
  }

  if (has(cJBOX_TOKEN)) {
    if(!checkCertValidity(getTokencertPath().c_str())) {
      msg += "Token certificate is invalid or expired and it should be renewed.\n";
    } else {
      preferedCredentials = cJBOX_TOKEN;
      return;
    }
  }

  if (has(cFULL_GRID_CERT)) {
    msg += "Fallback to full grid cert - please use alien-token-init or TGrid::Connect() to renew it.\n";
    preferedCredentials = cFULL_GRID_CERT;
    return;
  }

  msg += "Failed to find any credentials\n";
  preferedCredentials = cNOT_FOUND;
}

CredentialsKind TJAlienCredentials::getPreferedCredentials() const {
  return preferedCredentials;
}

const string& TJAlienCredentials::getMessages() const {
  return msg;
}

bool TJAlienCredentials::checkCertValidity(const char *path) {
  FILE *fCert = fopen(path, "r");
  bool result = false;
  X509 *x509 = NULL;
  ASN1_TIME *expire = NULL;
  using namespace std;

  // failed to open certificate file
  if(!fCert) {
    return false;
  }

  // if file exists but expired,
  char cert[4096];
  fread(cert, 1, 4096, fCert);
  fclose(fCert);

  BIO *b = BIO_new(BIO_s_mem());
  BIO_puts(b, cert);
  x509 = PEM_read_bio_X509(b, NULL, NULL, NULL);

  if(!x509) {
    result = false;
  } else {
    expire = X509_get_notAfter(x509);
  }

  // Failed to parse certificate expiration time
  if(expire != NULL) {
    if(!ASN1_TIME_check(expire)) {
      result = false;
    } else {
      int day, sec;

      // Compare certificate expiration time with current time
      ASN1_TIME_diff(&day, &sec, NULL, expire);
      result =  day > 0 || sec > 0;
    }
  }

  if(b) BIO_free(b);
  if(x509) X509_free(x509);

  return result;
}
